# Étape de construction
FROM maven:3.8.6-jdk-8-slim AS builder

# Définir le répertoire de travail
WORKDIR /usr/src/app

# Copier les fichiers du projet dans le conteneur
COPY . .

# Construire le projet et empaqueter l'application
RUN mvn clean package -DskipTests

# Étape finale
FROM openjdk:8-jre-slim

# Définir des variables d'environnement
ENV APP_HOME=/usr/src/app
ENV APP_JAR=app.jar

# Copier le fichier jar généré depuis l'étape de construction
COPY --from=builder ${APP_HOME}/target/*.jar /${APP_JAR}

# Exposer le port 8080
EXPOSE 8080

# Définir le point d'entrée de l'application
ENTRYPOINT ["java", "-jar", "/app.jar"]
